jquery.cycle.js
===============

- Version: 1.8
- Requires: jQuery v1.8.3
- Copywrite: (c) 2012 EYEMAGINE Technology, LLC
- Website: http://www.eyemaginetech.com

## Licence
- Licences: MIT, GPL
- http://www.opensource.org/licenses/mit-license.php
- http://www.gnu.org/licenses/gpl.html

# Example Usage:

    var myOptions = {
        slide: ".mySlide",
        animateInterval: 500,
        delay: 2500
    }

    $("#mySlider").jquerycycle(myOptions);
