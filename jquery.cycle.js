/**
 * jquery.cycle
 * Version: 1.8
 * Last Updated: January 14, 2013
 * Requires: jQuery v1.8.3
 *
 * Copywrite: (c) 2012 EYEMAGINE Technology, LLC
 * Website: http://www.eyemaginetech.com
 *
 * Licences: MIT, GPL
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 */
var jqueryCycles = new Array();
(function($) {
/******************************************************************************\

        // Example Usage:

        var myOptions = {
            slide: ".mySlide",
            animateInterval: 500,
            delay: 2500
        }

        $("#mySlider").jquerycycle(myOptions);

\******************************************************************************/

    $.fn.extend({

        jquerycycle: function(options) {

            $(this).each(function() {

                jqueryCycles.push(new jQueryCycle(this, options || {}));
            });
        }
    });

    /**
     * Creates a cycling banner
     *
     * @param   string  obj      Parent container of slider
     * @param   object  options  Options to override defaults (if any)
     *
     * @return  object           The jQueryCycle object
     */
    function jQueryCycle(obj, options) {

        // Default options
        this.options = {
            slide: ".slide", // The slide identifier to look for within obj
            animateInterval: 1000, // The speed that a slide animates att
            delay: 5000, // The delay between animations
            cycle: true, // Set automatic cycling
            animateStyle: 'slide',
            customNav: false,
            callback: false,
            stopOnClick: true,
            lrControls: {
                enabled: false,
                left: {
                    "default": {
                        background: "url('../images/banner/"
                        + "banner-lr-controls-bg.png') no-repeat top left",
                        width: 18,
                        height: 33,
                        left: 0,
                        top: "50%",
                        marginTop: -30,
                        position: "absolute",
                        cursor: "pointer"
                    },
                    hover: {
                        background: "url('../images/banner/"
                        + "banner-lr-controls-bg.png') no-repeat bottom left",
                        width: 18,
                        height: 33,
                        left: 0,
                        top: "50%",
                        marginTop: -30,
                        position: "absolute",
                        cursor: "pointer"
                    },
                    "class": "cycleNavLeft"
                },
                right: {
                    "default": {
                        background: "url('../images/banner/"
                        + "banner-lr-controls-bg.png') no-repeat top right",
                        width: 18,
                        height: 33,
                        right: 0,
                        top: "50%",
                        rotate: "180deg",
                        marginTop: -30,
                        position: "absolute",
                        cursor: "pointer"
                    },
                    hover: {
                        background: "url('../images/banner/"
                        + "banner-lr-controls-bg.png') no-repeat bottom right",
                        width: 18,
                        height: 33,
                        right: 0,
                        top: "50%",
                        rotate: "180deg",
                        marginTop: -30,
                        position: "absolute",
                        cursor: "pointer"
                    },
                    "class": "cycleNavRight"
                }
            }
        }

        // Overide default options with supplied, if any
        $.extend(true, this.options, options);
        this.obj = jQuery(obj);     // The parent object slider
        this.count = 0;             // Count variable to hold slide num
        this.slideWidth = parseInt(this.obj.width());
        this.slideHeight = parseInt(this.obj.height());
        this.index = 0;             // The index of the current slide
        this.lastIndex = 0;
        this.timer = null;          // hold the timeout obj

        // Kick it off
        this.init();
    }

    jQueryCycle.prototype = {

        addClone: function() {

            if (this.obj.find(".clone").length == 0) {

                this.obj.find(".cycleSlider").after(
                    this.obj.find(this.options.slide)
                    .eq(this.lastIndex).clone().css({
                        position: "absolute",
                        top: 0,
                        left: 0
                    }).addClass("clone")
                );
            }
        },
        /**
         * Create the nav controls for our slides
         */
        addNav: function() {

            var div = $("<ul>").addClass("cycleNav");

            // Foreach slide, create a nav for it
            for (var i = 0; i < this.count; i++) {

                var nav = $("<li>").addClass("cycleNavItem")
                .css("cursor", "pointer");

                if (this.options.customNav) {

                    nav.html(
                        this.slides.eq(i).find(this.options.slide)
                        .attr("data-nav")
                    );

                    this.slides.eq(i).find(this.options.slide)
                    .removeAttr("data-nav");
                }

                div.append(nav);
            }

            // Add the navs to our parent obj
            this.nav = div.appendTo(this.obj);
        },
        /**
         * Behavior of a single slide animation
         */
        animateToIndex: function() {

            var that = this;

            switch (that.options.animateStyle) {

                case 'slide':

                    if (that.count > 2) {

                        var clone = 0;

                        if (that.index == 0 && that.lastIndex == that.count - 1) {

                            that.addClone();
                            that.slider.css("left", that.slideWidth);
                            clone = -that.slideWidth;
                        }
                        else if (that.lastIndex == 0
                        && that.index == that.count - 1) {

                            that.addClone();
                            that.slider.css("left", -that.slideWidth * that.count);
                            clone = that.slideWidth;
                        }

                        if (clone != 0)
                        that.obj.find(".clone").animate({
                            left: clone
                        }, that.options.animateInterval, function() {

                            that.obj.find(".clone").remove()
                        });
                    }
                case 'slideTrue':

                    // Stop any current animations and then animate
                    that.slider.stop().animate({
                        left: -that.index * that.slideWidth
                    }, that.options.animateInterval, function() {

                        // When animation is complete, do it again
                        that.cycleOne();
                    });
                    break;
                case 'fade':

                    that.slides.filter(":visible")
                    .not(":eq(" + that.index + ")")
                    .stop(true).animate(
                        {opacity: 0},
                        that.options.animateInterval
                    );

                    that.slides.eq(that.index)
                    .stop(true).animate(
                        {opacity: 1},
                        that.options.animateInterval
                    );
                    
                    that.cycleOne();
                    break;
                default:

                    console.log("jquery.cycle: unknown animate style \""
                    + that.options.animateStyle + "\"");
            }
        },
        /**
         * Manipulate HTML markup to required specs
         */
        arrange: function() {

            var that = this,
            // Create a new slider container
            slider = $("<div>"),
            // Save this (jQueryCycle Object) to var for later use
            that = this;

            // Add class to slider container
            slider.addClass("cycleSlider").css("position", "absolute");

            // Foreach slide found within obj...
            this.obj.find(this.options.slide).each(function(i) {

                // Create a new slide
                var slide = $("<div>");

                // Increment our slide count
                that.count++;

                slide.addClass("cycleSlide").css("float", "left");

                // Remove the original slide HTML and add it to our new slide
                slide.append($(this).remove());

                // Force height and width
                slide.width(that.slideWidth);
                slide.height(that.slideHeight);

                if (that.options.animateStyle == 'fade') {

                    slide.css("position", "absolute");

                    if (i != 0) slide.animate({opacity: 0}, 1);
                }

                // Add the slide to our slider container
                slider.append(slide);
            });

            // Clear any uncaught HTML
            this.obj.empty;

            // Add the entire slider with slides to the parent obj
            this.slider = slider.appendTo(this.obj);
            this.slider.width(this.obj.width() * this.count);

            this.slides = this.obj.find(".cycleSlide");
        },
        /**
         * Trigger a single slide animation
         */
        cycleOne: function() {

            var that = this;

            // Create the delay between automatic cycles
            that.timer = setTimeout(function() {

                // If automatic cycling is on...
                if (that.options.cycle)
                    // increment the index by 1
                    that.moveAll(1, false);

            }, that.options.delay);
        },
        init: function() {

            this.obj.css(
                "position",
                this.obj.css("position") == "absolute" ? "absolute" : "relative"
            );
            this.arrange();
            this.addNav();
            this.setNav();
            this.lrControls();
            this.setEvents();
            this.cycleOne();
            if (this.options.callback) this.options.callback(this);
        },
        /**
         * Initialize the left and right buttons, if necessary
         */
        lrControls: function() {

            var that = this;

            if (that.options.lrControls.enabled) {

                that.options.lrControls.left.obj = $("<div />")
                .css(that.options.lrControls.left['default'])
                .addClass(that.options.lrControls.left['class'])
                .appendTo(that.obj);

                that.options.lrControls.right.obj = $("<div />")
                .css(that.options.lrControls.right['default'])
                .addClass(that.options.lrControls.right['class'])
                .appendTo(that.obj);

                that.options.lrControls.left.obj.hover(function() {

                    $(this).css(that.options.lrControls.left.hover);

                }, function() {

                    $(this).css(that.options.lrControls.left['default']);
                }).click(function() {

                    that.moveAll(-1);
                });

                that.options.lrControls.right.obj.hover(function() {

                    $(this).css(that.options.lrControls.right.hover);

                }, function() {

                    $(this).css(that.options.lrControls.right['default']);
                }).click(function() {

                    that.moveAll(1, true);
                });
            }
        },
        moveAll: function(move, stopAction, to) {

            stopAction = stopAction || false;
            console.log(move, stopAction, to);
            if (stopAction && this.options.cycle) {
                if (this.options.stopOnClick) this.options.cycle = false;
                else clearTimeout(this.timer);
            }
            this.moveIndex(move, to || false);
            this.setNav();
            this.animateToIndex();
        },
        /**
         * Increment, decrement, or set the current index with sanity checks
         */
        moveIndex: function(move, set) {

            set = set || false;
            this.lastIndex = this.index;

            if (set) return this.index = move;

            this.index += move;

            if (this.index < 0) this.index = this.count - 1;
            if (this.index > this.count - 1) this.index = 0;
        },
        /**
         * Captures click events
         */
        setEvents: function() {

            var that = this;

            // On click
            that.nav.children(".cycleNavItem").click(function() {

                // Set the index to the clicked object
                that.moveAll($(this).parent()
                .children(".cycleNavItem").index($(this)[0]), true, true);
            });
        },
        /**
         * Applies a class to the appropriate nav
         */
        setNav: function() {

            // Set the active nav controller to that of the current index
            this.nav.children(".cycleNavItem").removeClass("current")
            .eq(this.index).addClass("current");
        }
    }
})(jQuery);
